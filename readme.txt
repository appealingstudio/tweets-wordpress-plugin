Twitter feed which uses twitteroauth for authentication

Notes:
Caching is employed because Twitter only allows their RSS and json feeds to be accesssed 150
times an hour per user client.
--
Dates can be displayed in Twitter style (e.g. "1 hour ago") by setting the
$twitter_style_dates param to true.

You will also need to register your application with Twitter, to get your keys and tokens.
You can do this here: (https://dev.twitter.com/).

Don't forget to add your username to the bottom of the script.




